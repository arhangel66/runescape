# Base Uploader ===============================================================
import logging
import sys
from logging.handlers import RotatingFileHandler
from mimetypes import MimeTypes
from time import time

import re
import requests
from abc import ABCMeta
from bs4 import BeautifulSoup
from random import choice


class BaseGrabber(object):
    """ This is an abstract class that contains required and optional
    predefined methods that are used by all uploaders """
    proxy_list = [
        '155.94.166.55:80',
        '155.94.166.91:80',
        '104.144.5.3:80',
        # '45.61.37.191:80',
        # '45.61.37.7:80',
        '104.144.5.61:80',
        '104.144.5.73:80',
        # '45.61.37.115:80',
        # '23.247.237.87:80',
        '155.94.166.198:80',
        '23.108.2.21:80',
        '66.248.219.74:80',
        '23.108.2.13:80',
        '23.108.2.151:80'
    ]
    __metaclass__ = ABCMeta

    user_agents = [
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11',
        'Opera/9.25 (Windows NT 5.1; U; en)',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
        'Mozilla/5.0 (compatible; Konqueror/3.5; Linux) KHTML/3.5.5 (like Gecko) (Kubuntu)',
        'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20070731 Ubuntu/dapper-security Firefox/1.5.0.12',
        'Lynx/2.8.5rel.1 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/1.2.9'
    ]
    USER_AGENT = choice(user_agents)
    # "Mozilla/5.0 (X11; Linux x86_64; rv:24.0) Gecko/20140610 " \
    #   "Firefox/24.0 Iceweasel/24.6.0"

    CAT_API = "http://www.cliphunter.com/a/guesstags"

    # Methods -----------------------------------------------------------------

    def save_html(self, html, url=''):
        self.logger.info(html[:500])

    def set_request_proxy(self, proxy=None):
        """
        Set default proxy for requests (post and get)
        :param proxy: if proxy is None - then we will use random proxy from proxy_list
        :return:
        """
        is_good_proxy = False
        retries = 0
        while not is_good_proxy and retries < 5:
            retries += 1
            if type(proxy) is not str or retries > 1:
                proxy = choice(self.proxy_list)

            self.request_proxy = {
                'http': proxy,
                'https': proxy
            }
            self.logger.info('set default proxy for request as %s' % proxy)
            try:
                self.session.get("http://ipecho.net/plain")
                is_good_proxy = True
            except:
                self.logger.info('get error, when checking proxy, try to get next proxy')
                pass

    def unset_request_proxy(self):
        """
        unset default proxy
        :return:
        """
        self.request_proxy = None
        self.logger.info('unset default proxy for request')

    def session_post(self, url, data=None, **kwargs):
        # remove password from log
        try:
            print_data = dict(data)
            for key in print_data.keys():
                if 'pass' in key:
                    print_data[key] = '*' * len(print_data[key])
        except:
            print_data = data

        self.logger.info('post. url: %s, data: %s' % (url, print_data))

        # if we have default request_proxy and don't have proxy in that query - then we set proxy from default
        if not 'proxies' in kwargs and self.request_proxy:
            kwargs['proxies'] = self.request_proxy
        t2 = time()
        result = self.session.real_post(url, data=data, **kwargs)
        length = time() - t2
        self.logger.info('result:')
        self.logger.info('result ok: %s' % result.ok)
        self.logger.info('status: %s' % result.status_code)
        self.logger.info('len(result text): %s' % len(result.text))
        self.logger.info('time: %s' % length)
        if len(result.text) < 250:
            self.logger.info('result text:\n %s' % self.save_html(result.text, url))
        return result

    def session_get(self, url, **kwargs):
        # remove password from log
        self.logger.info('get. url: %s' % url)

        # if we have default request_proxy and don't have proxy in that query - then we set proxy from default
        if not 'proxies' in kwargs and self.request_proxy:
            kwargs['proxies'] = self.request_proxy
        t2 = time()
        result = self.session.real_get(url, **kwargs)
        length = time() - t2
        self.logger.info('result:')
        self.logger.info('result ok: %s' % result.ok)
        self.logger.info('status: %s' % result.status_code)
        self.logger.info('len(result text): %s' % len(result.text))
        self.logger.info('time: %s' % length)
        if len(result.text) < 250:
            self.logger.info('result text:\n %s' % self.save_html(result.text, url))
        return result

    def __init__(self, logfile=None, log=True, logger=None, headless=True):
        """ Initialize base uploader """
        self.id = ''
        self.request_proxy = None
        # Toggle headless
        self.headless = headless
        # Setup logger
        if logger:
            self.logger = logger
        else:
            if log:
                self.logger = self.setup_logger(logfile)
            else:
                self.logger = self.setup_logger('')
        if log:
            self.logger.disabled = not log

        self.last_screen = ''
        # Create a new session
        from requests.packages.urllib3.exceptions import InsecureRequestWarning
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        self.session = requests.Session()
        # print type(self.session.real_post)
        # print type(self.session.post)
        self.session.real_post = self.session.post
        self.session.post = self.session_post
        self.session.real_get = self.session.get
        self.session.get = self.session_get

        # Disable Keep-alive
        # self.session.keep_alive = False
        self.session.verify = False
        # Spoof user agent
        self.session.headers.update({'User-Agent': self.USER_AGENT,
                                     'language': 'en-US',
                                     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                                     'Accept-Language': 'ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3',
                                     'Content-Type': 'application/x-www-form-urlencoded',
                                     'Connection': 'Keep-Alive',
                                     'Accept-Charset': 'windows-1251,utf-8;q=0.7,*;q=0.7'
                                     })

        # Deathbycaptcha
        # self.dbc = deathbycaptcha.SocketClient("", "")

        # Initialize mime types
        self.mime = MimeTypes()

    # --------------------------------------------------------------------------

    def clean_log_handlers(self):
        """Close Log files and remove FileHandlers"""
        try:
            handlers = self.logger.handlers[:]
            for handler in handlers:
                handler.close()
                self.logger.removeHandler(handler)
            return True
        except:
            print('error in clean_log_handlers')
            return False

    # --------------------------------------------------------------------------

    @staticmethod
    def strip_field(field, regexp='[^a-zA-Z0-9.,? ]', delim=' ', max_length=-1):
        stripped = re.sub(regexp, '', field)
        if max_length > 0:
            words = []
            length = 0
            for word in stripped.split(delim):
                length += len(word) + 1
                if length >= max_length:
                    break
                words.append(word)
            return delim.join(words)
        return stripped

    # -------------------------------------------------------------------------

    def _random_wait(self, sec=1.5, min=0.5):
        import random
        import time
        wait = round(random.uniform(min, sec) * 100) / 100
        time.sleep(wait)
        return True

    # -------------------------------------------------------------------------

    def setup_logger(self, logfile=None):
        """ Setup a logger """

        # Init logger
        logger = logging.getLogger(self.__class__.__name__)
        logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - '
                                      '%(levelname)s - %(message)s')

        # Stdout handler
        try:
            handler = logging.StreamHandler(sys.stdout)
            handler.setFormatter(formatter)
            logger.addHandler(handler)
        except IOError:
            try:
                logger.addHandler(logging.NullHandler())
            except IOError:
                pass

        # Rotating file logs
        if logfile:
            try:
                handler = RotatingFileHandler(logfile, maxBytes=10 * 1024 * 1024, backupCount=5)
                handler.setFormatter(formatter)
                logger.addHandler(handler)
            except IOError:
                pass

        return logger

    @staticmethod
    def scrape_form(response, attrs):
        """ Scrapes form's hidden data """

        data = {}
        soup = BeautifulSoup(response, "html.parser")
        form = soup.find("form", attrs)
        if form:
            fields = form.findAll("input", {"type": "hidden",
                                            'name': True,
                                            'value': True})
            if fields:
                for field in fields:
                    name = field['name']
                    value = field['value']

                    if value:
                        if '[]' in name:
                            if name in data:
                                data[name].append(value)
                            else:
                                data[name] = [value]
                        else:
                            data[name] = value

        return data
