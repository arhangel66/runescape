# Install
1. Get python windows installer and install it https://www.python.org/downloads/release/python-342/
2. in command line open folder with script
3. write ```pip install -r requirements.txt```
4. check file settings.py with settings for login and pushover
5. in command line write ```python run.py``` . After it script will login to site (only once). When you will change names.txt file - script will set that name on server and send result to pushover.


## Log
You can find logs in same folder in file logs.txt


## Results
What script can return:
* Error! Your login or password was incorrect. Please try again.
* Purchase RuneScape membership
* There was an unknown error (script will try 3 times send post message if it will get that error)
* have to relogin
