import os
import sys
import time
import datetime

import settings
from script import RunEscape

from watchdog.events import LoggingEventHandler, FileSystemEventHandler
from watchdog.observers import Observer

directory = os.path.dirname(os.path.abspath(__file__))
filename = 'names7.txt'
full_name = '%s\%s' % (directory, filename)
delay_time = 20  # time of delay

def send_message(text):
    from chump import Application
    app = Application(settings.PUSHOVER_TOKEN)
    user = app.get_user(settings.PUSHOVER_USER)
    print(text)
    message = user.send_message(text)


def start(worker=None):
    """
    Function for set_name
    :return:
    """
    from script import RunEscape
    res = {}
    t2 = time.time()
    if not worker:
        worker = RunEscape(logfile='logs.txt')

    with open(full_name) as file:
        new_name = file.read()

    if worker.id:
        # try to send name without login
        res = worker.set_name(new_name)
        worker.logger.info("set_name_res: %s" % res)

    if not worker.id or res['message'] == 'have to relogin':
        res = worker.login(settings.EMAIL, settings.PASSWORD)
        worker.logger.info("login_res: %s" % res)

        if res['result']:
            res = worker.set_name(new_name)
            worker.logger.info("set_name_res: %s" % res)
            print(time.time() - t2)

    if res['result']:
        message = "Success! %s has been set to %s!" % (new_name, settings.EMAIL)
        # save to success_log.txt file
        log_message = "%s / %s / %s " % (
        new_name, settings.EMAIL, datetime.datetime.now().strftime("%d/%m/%Y / %I:%M%p"))
        with open("success_log.txt", "a+") as myfile:
            myfile.write(log_message)
    else:
        message = "SuperSniper has failed to retrieve %s because: \n %s" % (new_name, res['message'])
    send_message(message)


class MyHandler(FileSystemEventHandler):
    patterns = ["*.xml", "*.lxml"]
    last_update = None

    def process(self, event):
        """
        event.event_type
            'modified' | 'created' | 'moved' | 'deleted'
        event.is_directory
            True | False
        event.src_path
            path/to/observed/file
        """
        # the file will be processed there
        print(event.src_path)
        if filename in event.src_path:
            if not self.last_update or datetime.datetime.now() - self.last_update > datetime.timedelta(seconds=delay_time):
                if self.last_update:
                    print(datetime.datetime.now() - self.last_update)
                self.last_update = datetime.datetime.now()
                start(worker)

    def on_modified(self, event):
        self.process(event)

        # def on_created(self, event):
        #     self.process(event)


if __name__ == "__main__":
    event_handler = LoggingEventHandler()
    observer = Observer()

    # login to site
    worker = RunEscape(logfile='logs.txt')
    res = worker.login(settings.EMAIL, settings.PASSWORD)
    worker.logger.info("login_res: %s" % res)
    observer.schedule(MyHandler(), directory, recursive=False)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
