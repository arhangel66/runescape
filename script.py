import json
from pprint import pprint
from time import sleep

import re

from base_grabber import BaseGrabber
from bs4 import BeautifulSoup


class RunEscape(BaseGrabber):
    LOGIN_PAGE = "https://secure.runescape.com/m=weblogin/loginform.ws?mod=www&ssl=1&reauth=1&dest=account_settings.ws?jptg=ia&jptv=navbar"
    LOGIN_URL = "https://secure.runescape.com/m=weblogin/login.ws"
    CHECK_NAME_URL = "https://secure.runescape.com/m=displaynames/c=__ID__/check_name.ws?displayname=__NAME__"
    SET_NAME_URL = "https://secure.runescape.com/m=displaynames/c=__ID__/name.ws"

    def login(self, username, password):
        """ Login to the website using the specified credentials """

        result = {'result': False, 'message': ''}
        response = self.session.get(self.LOGIN_PAGE)
        if response.ok:
            # Prepare payload
            payload = self.scrape_form(response.text,
                                       {'class': 'login-form'})
            payload['username'] = username
            payload['password'] = password

            # Post payload
            response = self.session.post(self.LOGIN_URL,
                                         data=payload)

            # check result of post:
            if 'Sign Out' in response.text:
                # get ID
                try:
                    self.id = re.findall(r'c=([^/]+)/', response.url)[0]
                    result['message'] = 'id: %s' % self.id
                except:
                    self.logger.error('cant get ID from url: %s' % response.url)
                    pass

                result['result'] = True
            else:
                soup = BeautifulSoup(response.text, "html.parser")
                error = soup.find("p", {'class': 'component-callout--type-error'})
                result['message'] = error.text

            return result


    def set_name(self, new_name, count=0):
        """

        :param new_name:
        :return:
        """
        result = {'result': False, 'message': ''}

        # Check name
        # url = self.CHECK_NAME_URL.replace('__ID__', self.id).replace('__NAME__', new_name)
        # headers = {
        #     'Host': 'secure.runescape.com',
        #     'Referer': 'https://secure.runescape.com/m=displaynames/c=%s/name.ws' % self.id,
        # }
        # response = self.session.get(url, headers=headers)
        # if response.text != 'OK':
        #     result['message'] = 'Name is not avaiable'
        #     return result
        # else:
        #     self.logger.info('Name %s is available' % new_name)

        # set name
        url = self.SET_NAME_URL.replace('__ID__', self.id)
        headers = {'Origin': 'https://secure.runescape.com',
                   'Upgrade-Insecure-Requests': '1',
                   'Host': 'secure.runescape.com',
                   'Referer': 'https://secure.runescape.com/m=displaynames/c=%s/name.ws' % self.id,
                   }
        payload = {
            'name': new_name,
            'ssl': '-1',
            'confirm': 'Yes'
        }
        response = self.session.post(url, payload, headers=headers)

        # check results
        # print(response.text)
        if '>%s</b>' % new_name in response.text:
            self.logger.info('Change name success')
            result['result'] = True
            result['message'] = 'Success'
        elif 'Purchase RuneScape membership' in response.text:
            self.logger.error('Need pay')
            result['message'] = "Purchase RuneScape membership"
        elif 'There was an unknown error' in response.text:
            self.logger.error('There was an unknown error, will try again')
            if count < 3:
                sleep(1)
                self.set_name(new_name, count=count + 1)
        elif 'Log In' in response.text:
            result['message'] = 'have to relogin'
        elif str(response.status_code) == '404':
            result['message'] = 'have to relogin'

        return result
